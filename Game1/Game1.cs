﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace Game1
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //Load game assets
        Texture2D buttontexture;
        SpriteFont gamefont;
        SoundEffect clickSFX;

        //gamestate/ input
        MouseState PreviousState;
        int Score = 0;
        bool start = false;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            //load button Graphic
            buttontexture = Content.Load<Texture2D>("graphics/button");

            //load gamefont
            gamefont = Content.Load<SpriteFont>("fonts/mainFont");

            //load sfx
            clickSFX = Content.Load<SoundEffect>("audio/buttonclick");

            //load mosue
            IsMouseVisible = true;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            //get current Mouse State
            MouseState currentState = Mouse.GetState();

            //find center of screen
            Vector2 screenCenter = new Vector2(Window.ClientBounds.Width / 2, Window.ClientBounds.Height / 2);

            
            //determine button rectangle
            Rectangle buttonRectangle = new Rectangle(
                (int)screenCenter.X - buttontexture.Width,
                (int)screenCenter.Y - buttontexture.Height,
                buttontexture.Width,
                buttontexture.Height);

            //check if we have clicked the mouse
            if (currentState.LeftButton == ButtonState.Pressed
                && PreviousState.LeftButton != ButtonState.Pressed
                && buttonRectangle.Contains(currentState.X, currentState.Y))
            {
                //mouse is pressed
                clickSFX.Play();
                if (start == true)
                {
                    //add to score
                    ++Score;
                }

                //set start to true
                start = true;
            }

            //current state becomes previous state
            PreviousState = currentState;

            base.Update(gameTime);
        }

   
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            //Find the Center of the screen
            Vector2 screenCenter = new Vector2(Window.ClientBounds.Width / 2, Window.ClientBounds.Height / 2);

            //Start Drawing
            spriteBatch.Begin();

            //draw
            spriteBatch.Draw(buttontexture, 
                new Rectangle(
                (int)screenCenter.X -buttontexture.Width,
                (int)screenCenter.Y - buttontexture.Height, 
                buttontexture.Width, 
                buttontexture.Height), 
                Color.White
                );

            //draw text
            Vector2 TitleSize = gamefont.MeasureString("Button Masher");

            spriteBatch.DrawString(
                gamefont,
                "ButtonMasher",
                screenCenter - new Vector2(0, 100) - TitleSize/2,
                Color.White);

            //draw text
            Vector2 AuthorName = gamefont.MeasureString("by logan Weeden");

            spriteBatch.DrawString(
                gamefont,
                "By Logan Weeden",
                screenCenter - new Vector2(0, 75) - TitleSize / 2,
                Color.White);

            string promtString = "click the button to start";
            if (start == true)
            {
                promtString = "click sa fast as you can before time runs out";
            }

            Vector2 promptSize = gamefont.MeasureString(promtString);

            spriteBatch.DrawString(
                gamefont,
                "Click the button to start",
                screenCenter - new Vector2(0, 60) - TitleSize / 2,
                Color.White);

            spriteBatch.DrawString(
                gamefont,
                "Score" ,
                new Vector2(10,10) ,
                Color.White);

            spriteBatch.DrawString(
                gamefont,
                Score.ToString(),
                new Vector2(100, 10),
                Color.White);


            //Stop Drawing
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
